import json
import subprocess
import webbrowser
import os
import requests
import shutil
import hashlib


class ContentCompare(object):
    def __init__(self):
        """
        Constructor
        """
        self.prereq = ContentCompare.load_prerequisites()

        self.inputs_dir = 'inputs'
        if os.path.isdir(self.inputs_dir):
            shutil.rmtree(self.inputs_dir)
        os.mkdir(self.inputs_dir)

        self.outputs_dir = 'outputs'
        if os.path.exists(self.outputs_dir):
            shutil.rmtree(self.outputs_dir)
        os.mkdir(self.outputs_dir)

        self.bkp_dir = 'backups'
        if not os.path.exists(self.bkp_dir):
            os.mkdir(self.bkp_dir)

        self.file_names = []
        for item in self.prereq['urls']:
            base_file_url = item['base_file']
            recent_file_url = item['recent_file']

            # Split url to get filename
            file_name_ext = os.path.basename(base_file_url)
            file_name, ext = os.path.splitext(file_name_ext)
            base_file = self.inputs_dir + '/' + file_name + '_base.txt'
            recent_file = self.inputs_dir + '/' + file_name + '_recent.txt'

            # Get base content
            resp = requests.get(base_file_url, verify=False,
                                auth=(self.prereq["auth"][0], self.prereq["auth"][1]))  # stream=True,
            with open(base_file, 'w') as fp_base:
                fp_base.write(resp.content)

            # Get recent content
            resp = requests.get(recent_file_url, verify=False,
                                auth=(self.prereq["auth"][0], self.prereq["auth"][1]))  # stream=True,
            with open(recent_file, 'w') as fp_recent:
                fp_recent.write(resp.content)

            # Collect file names
            self.file_names.append(file_name)

    @staticmethod
    def load_prerequisites():
        """
        Load settings
        :return: json
        """
        with open('prerequisites.json') as fp:
            return json.load(fp)

    @staticmethod
    def is_files_identical(filename1, filename2):
        """
        Check the both files hashes are equal 
        :param filename1: str
        :param filename2: str
        :return: bool
        """
        hash1 = hash2 = ''
        try:
            with open(filename1, 'rb') as fb1:
                buf = fb1.read()
                hasher = hashlib.md5()
                hasher.update(buf)
                hash1 = hasher.hexdigest()
        except IOError, e:
            print filename1 + ' does not exist'
            # return False

        try:
            with open(filename2, 'rb') as fb2:
                buf = fb2.read()
                hasher = hashlib.md5()
                hasher.update(buf)
                hash2 = hasher.hexdigest()
        except IOError, e:
            print filename2 + ' does not exist'
            # Trick: first run last destination backup file does not exist, so we copy it from outputs.
            # Following re-run is need.
            shutil.copy(filename1, filename2)
            return True

        return len(hash1) > 0 and len(hash2) > 0 and hash1 == hash2

    # def is_relevant_content_was_changed(self):
    #     """
    #     Check if the recent relevant content was changed since last build
    #     :return: bool
    #     """
    #     dest_recent_bkp_file = self.prereq['dest_recent_file_bkp']
    #     changed_resent = not self.is_files_identical(self.dest_recent_file, dest_recent_bkp_file)
    #     changed_prereq = not self.is_files_identical('prerequisites.json', 'prerequisites_bkp.json')
    #     changed = changed_resent or changed_prereq
    #
    #     # Save the result is identical (True/False) to text file for outside usage (e.g. by Jenkins)
    #     workspace_dir = os.getenv('WORKSPACE', os.getcwd())
    #     print 'WORKSPACE:', workspace_dir
    #     file_name = os.getenv('TRANK_CHANGED', 'trank_changed.txt')
    #     print 'exchange file:', file_name
    #     file_path = os.path.join(workspace_dir, file_name)
    #     with open(file_path, 'w') as f:
    #         f.write(str(changed))
    #
    #     return changed_resent, changed_prereq

    @staticmethod
    def save_diff_result_to_ext_file(changed):
        """
        Save the diff result (True/False) to text file for outside usage (e.g. by Jenkins)
        :param changed: bool
        """
        workspace_dir = os.getenv('WORKSPACE', os.getcwd())
        file_name = os.getenv('TRANK_CHANGED', 'trank_changed.txt')
        file_path = os.path.join(workspace_dir, file_name)
        with open(file_path, 'w') as f:
            f.write(str(changed))

    def compare_relevant_content(self):

        """
        Get diff recent - base and resent - last build accordingly.
        Save the diff to two HTMLs
        :return:
        """
        changed_prereq = not self.is_files_identical('prerequisites.json', self.bkp_dir + '/prerequisites_bkp.json')
        ContentCompare.save_diff_result_to_ext_file(changed_prereq)  # Initial save

        for file_name in self.file_names:
            base_file = self.inputs_dir + '/' + file_name + '_base.txt'
            recent_file = self.inputs_dir + '/' + file_name + '_recent.txt'
            recent_file_bkp = self.bkp_dir + '/' + file_name + '_recent_bkp.txt'
            base_diff_txt = self.outputs_dir + '/' + file_name + '_base_diff.txt'
            last_diff_txt = self.outputs_dir + '/' + file_name + '_last_diff.txt'
            base_diff_html = self.outputs_dir + '/' + file_name + '_base_diff.html'
            last_diff_html = self.outputs_dir + '/' + file_name + '_last_diff.html'
            base_diff_html_bkp = self.bkp_dir + '/' + file_name + '_base_diff_bkp.html'
            last_diff_html_bkp = self.bkp_dir + '/' + file_name + '_last_diff_bkp.html'

            # Check if recent (most likely SVN Trank) content was changed
            changed_resent = not self.is_files_identical(recent_file, recent_file_bkp)
            if not changed_resent and not changed_prereq:
                print '{} did not changed - nothing compare.'.format(file_name)

                # Restore the both diff.html from backup
                if os.path.exists(base_diff_html_bkp):
                    shutil.copy(base_diff_html_bkp, base_diff_html)
                if os.path.exists(last_diff_html_bkp):
                    shutil.copy(last_diff_html_bkp, last_diff_html)
                continue

            # Call the 'diff' utility and convert the results to HTML files
            with open(base_diff_txt, 'w') as f_base_diff_txt:
                subprocess.call(['diff', '-u', base_file, recent_file], stdout=f_base_diff_txt)
            subprocess.call(["diff2html", "--input", "file", base_diff_txt, "--style", "side",
                             "--file", base_diff_html])

            with open(last_diff_txt, 'w') as f_last_diff_txt:
                if changed_prereq:
                    f_last_diff_txt.write('Paths are changed - compare does not relevant')
                elif os.path.exists(recent_file_bkp):
                    subprocess.call(['diff', '-u', recent_file_bkp, recent_file], stdout=f_last_diff_txt)

            # last_diff.txt may be empty - check it
            with open(last_diff_txt, 'r') as last_txt:
                lines_last = last_txt.readlines()
            if len(lines_last) == 0:
                with open(last_diff_txt, 'w') as last_txt:
                    last_txt.write('No changes')
            subprocess.call(["diff2html", "--input", "file", last_diff_txt, "--style", "side",
                             "--file", last_diff_html])

            # Update changed_recent == True in ext file
            ContentCompare.save_diff_result_to_ext_file(True)

            # Backups for the next run
            shutil.copy(recent_file, recent_file_bkp)
            shutil.copy('prerequisites.json', self.bkp_dir + '/prerequisites_bkp.json')
            shutil.copy(base_diff_html, base_diff_html_bkp)
            shutil.copy(last_diff_html, last_diff_html_bkp)

            if not os.getenv('WORKSPACE'):
                path_base_html = os.path.abspath(base_diff_html)
                print path_base_html
                webbrowser.get('/usr/bin/google-chrome').open(path_base_html)
                path_last_html = os.path.abspath(last_diff_html)
                print path_last_html
                webbrowser.get('/usr/bin/google-chrome').open(path_last_html)


def main():
    cmpr = ContentCompare()
    # cmpr.extract_relevant_content()
    cmpr.compare_relevant_content()

if __name__ == "__main__":
    main()


